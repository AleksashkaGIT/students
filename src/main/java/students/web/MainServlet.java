package students.web;
import students.dao.*;
import students.dao.mysql.*;
import students.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
public class MainServlet extends HttpServlet {
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private SubjectDao subjectDao;
    @Autowired
    private MarkDao markDao;
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s = request.getParameter("button");
        switch (s) {
            case "Edit the list of students":
                listStudent(request, response);
                break;
            case "Edit the list of subjects":
                listSubject(request, response);
                break;
            case "Add student":
                addStudent(request, response);
                break;
            case "Add subject":
                addSubject(request, response);
                break;
            case "Appoint subject":
                appointSubject(request, response);
                break;
            case "Update student":
                updateStudent(request, response);
                break;
            case "Update subject":
                updateSubject(request, response);
                break;
            case "Delete student":
                deleteStudent(request, response);
                listStudent(request, response);
                break;
            case "Delete subject":
                deleteSubject(request, response);
                listSubject(request, response);
                break;
        }
    }
    private void listStudent(HttpServletRequest request, HttpServletResponse response){
        try {
            request.setAttribute("listStudent",studentDao.getAll());
            request.getRequestDispatcher("/listStudent.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void listSubject(HttpServletRequest request, HttpServletResponse response){
        try {
            request.setAttribute("listSubject", subjectDao.getAll());
            request.getRequestDispatcher("/listSubject.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void addStudent(HttpServletRequest request, HttpServletResponse response){
        try {
            if(request.getParameter("firstname").matches("[A-Za-z]{2,}") && request.getParameter("secondname").matches("[A-Za-z]{2,}") && request.getParameter("birthday").matches("((19|20)\\d{2})-(0[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])") && request.getParameter("year").matches("(19|20)\\d{2}")) {
                studentDao.create(new Student(0, request.getParameter("firstname"), request.getParameter("secondname"), request.getParameter("birthday"), Integer.parseInt(request.getParameter("year"))));
				response.getWriter().println("Student successfully added to the Database!");
			} else {
                response.getWriter().println("Student has not been added to the Database, input correct data!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void addSubject(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameter("subjectname").matches("[A-Za-z]{2,}")) {
                if(subjectDao.getByName(request.getParameter("subjectname"))== null) {
                    subjectDao.create(new Subject(0, request.getParameter("subjectname")));
                    request.setAttribute("messageAddSub", "Subject successfully added to the Database!");
                } else {
                    request.setAttribute("messageAddSub", "This subject has been added to the Database before.");
                }
            } else {
                request.setAttribute("messageAddSub", "Subject has not been added to the Database, input correct data!");
            }
            request.getRequestDispatcher("/addSubject.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void appointSubject(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameter("subjectname").matches("[A-Za-z]{2,}")) {
                Subject subject = subjectDao.getByName(request.getParameter("subjectname"));
                if(subject == null) {
                    request.setAttribute("result", "Subject is not found in the Database.");
                }else {
                    markDao.create(new Mark(0, studentDao.getByPK(Integer.parseInt(request.getParameter("idSt"))), subject, 0));
                    request.setAttribute("result", "Subject successfully assigned to the Student!");
                }
            } else {
                request.setAttribute("result", "Subject has not been assigned to the Student in Database, input correct data!");
            }
            request.getRequestDispatcher("/appointSubject.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void updateStudent(HttpServletRequest request, HttpServletResponse response) {
        try {
            if(request.getParameter("firstname").matches("[A-Za-z]{2,}") && request.getParameter("secondname").matches("[A-Za-z]{2,}") && request.getParameter("birthday").matches("((19|20)\\d{2})-(0[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])") && request.getParameter("year").matches("(19|20)\\d{2}")) {
                Student student = studentDao.getByPK(Integer.parseInt(request.getParameter("idStudent")));
				student.setFirstName(request.getParameter("firstname"));
				student.setSecondName(request.getParameter("secondname"));
				student.setBirthDate(request.getParameter("birthday"));
				student.setEnterYear(Integer.parseInt(request.getParameter("year")));
				studentDao.update(student);
                response.getWriter().println("Student successfully updated in the Database!");
            } else {
                response.getWriter().println("Student has not been updated, input correct data!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void updateSubject(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameter("subjectname").matches("[A-Za-z]{2,}")) {
				Subject subject = subjectDao.getByPK(Integer.parseInt(request.getParameter("idSubj")));
				subject.setSubjectName(request.getParameter("subjectname"));
                subjectDao.update(subject);
                request.setAttribute("messageUpdSub", "Subject successfully updated in the Database!");
            } else {
                request.setAttribute("messageUpdSt", "Subject has not been updated, input correct data!");
            }
            request.getRequestDispatcher("/updateSubject.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void deleteStudent(HttpServletRequest request, HttpServletResponse response) {
        try {
             studentDao.delete(Integer.parseInt(request.getParameter("idStud")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void deleteSubject(HttpServletRequest request, HttpServletResponse response) {
        try {
            subjectDao.delete(Integer.parseInt(request.getParameter("idSub")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}