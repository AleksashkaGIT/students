package students.web;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import students.dao.*;
import students.dao.mysql.*;
import org.hibernate.Session;
public class MainSessionListener implements HttpSessionListener {
	private StudentDao studentDao;
    private SubjectDao subjectDao;
    private MarkDao markDao;
    public void sessionCreated(HttpSessionEvent event){
        try {
            MySqlDaoFactory factory = new MySqlDaoFactory();
            Session session = factory.getSession();
            studentDao = factory.getStudentDAO(session);
            subjectDao = factory.getSubjectDAO(session);
            markDao = factory.getMarkDAO(session);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
	public void sessionDestroyed(HttpSessionEvent event) {
        DAOException exc = null;
        try {
            studentDao.close();
        } catch (DAOException d){
            exc = d;
        }
        try {
            subjectDao.close();
        } catch (DAOException d){
            if(exc == null) {
                exc = d;
            } else {
                System.err.println("SubjectDao is not closed");
            }
        }
        try {
            markDao.close();
        } catch (DAOException d){
            if(exc == null) {
                exc = d;
            } else {
                System.err.println("MarkDao is not closed");
            }
        }
        if (exc != null) {
            System.err.println("Closing error");
            exc.printStackTrace();
        }
    }
}