package students.dao;
import students.domain.Mark;
import java.util.List;
public interface MarkDao{
    void create(Mark mark)throws DAOException;
    Mark getByPK(int key)throws DAOException;
    void update(Mark mark)throws DAOException;
    void delete(int key)throws DAOException;
    List<Mark> getAll()throws DAOException;
    void close()throws DAOException;
}