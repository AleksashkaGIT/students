package students.dao;
import students.domain.Subject;
import java.util.List;
public interface SubjectDao {
    void create(Subject subject)throws DAOException;
    Subject getByPK(int key)throws DAOException;
    void update(Subject subject)throws DAOException;
    void delete(int key)throws DAOException;
    List<Subject> getAll()throws DAOException;
    Subject getByName(String subjectName) throws DAOException;
    void close()throws DAOException;
}