package students.dao.mysql;
import students.dao.DAOException;
import students.dao.StudentDao;
import students.domain.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;
public class MySqlStudentDao implements StudentDao, AutoCloseable {
    private Session session;
    public MySqlStudentDao(Session session){
        this.session = session;
    }
    public Student getByPK(int key) throws DAOException {
        Student student;
        try{
            student = (Student)session.get(Student.class, key);
        }catch (Exception e) {
            throw new DAOException("Error in getting student by PK",e);
        }
        return student;
    }
     public void update(Student student) throws DAOException {
        try {
			Transaction transaction = session.beginTransaction();
            session.update(student);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in student update", e);
        }
    }
    public void delete(int key) throws DAOException {
        try {
			Transaction transaction = session.beginTransaction();
            session.delete((Student)session.get(Student.class, key));
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in student delete", e);
        }
    }
    public List<Student> getAll() throws DAOException{
        List<Student> list;
        try {
            list = session.createQuery("from Student").list();
        } catch (Exception e) {
            throw new DAOException("Error in get all students", e);
        }
        return list;
    }
    public void create(Student student) throws DAOException {
        try {
			Transaction transaction = session.beginTransaction();
            session.save(student);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in creating student", e);
        }
    }
    public void close() throws DAOException {
        try {
            session.close();
        } catch (Exception e) {
            throw new DAOException("Resources is not closed", e);
        }
    }
}