package students.dao.mysql;
import students.dao.DAOException;
import students.dao.MarkDao;
import students.domain.Mark;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;
public class MySqlMarkDao implements MarkDao, AutoCloseable {
    private Session session;
    public MySqlMarkDao(Session session) {
        this.session = session;
    }
    public Mark getByPK(int key) throws DAOException {
        Mark mark;
        try{
            mark = (Mark)session.get(Mark.class, key);
        }catch (Exception e) {
            throw new DAOException("Error in getting mark by PK",e);
        }
        return mark;
    }
    public void update(Mark mark) throws DAOException {
        try {
            Transaction transaction = session.beginTransaction();
            session.update(mark);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in mark update", e);
        }
     }
    public void delete(int key) throws DAOException {
        try {
            Transaction transaction = session.beginTransaction();
            session.delete(session.get(Mark.class, key));
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in mark delete", e);
        }
    }
    public List<Mark> getAll() throws DAOException {
        List<Mark> list;
        try {
            list = session.createQuery("from Mark").list();
        } catch (Exception e) {
            throw new DAOException("Error in get all marks", e);
        }
        return list;
    }
    public void create(Mark mark) throws DAOException {
        try {
            Transaction transaction = session.beginTransaction();
            session.save(mark);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in creating mark", e);
        }
    }
    public void close() throws DAOException {
        try {
            session.close();
        } catch (Exception e) {
            throw new DAOException("Resources is not closed", e);
        }
    }
}