package students.dao.mysql;
import students.dao.DAOException;
import students.dao.SubjectDao;
import students.domain.Subject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;
public class MySqlSubjectDao implements SubjectDao, AutoCloseable {
    private Session session;
    public MySqlSubjectDao(Session session) {
        this.session = session;
    }
    public Subject getByPK(int key) throws DAOException {
        Subject subject;
        try{
            subject = (Subject)session.get(Subject.class, key);
        }catch (Exception e) {
            throw new DAOException("Error in getting subject by PK",e);
        }
        return subject;
    }
    public void update(Subject subject) throws DAOException {
        try {
			Transaction transaction = session.beginTransaction();
            session.update(subject);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in subject update", e);
        }
    }
    public void delete(int key) throws DAOException {
        try {
            Transaction transaction = session.beginTransaction();
            session.delete(session.get(Subject.class, key));
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in subject delete", e);
        }
    }
    public List<Subject> getAll() throws DAOException{
        List<Subject> list;
        try {
            list = session.createQuery("from Subject").list();
        } catch (Exception e) {
            throw new DAOException("Error in get all subjects", e);
        }
        return list;
    }
    public void create(Subject subject) throws DAOException {
        try {
            Transaction transaction = session.beginTransaction();
            session.save(subject);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException("Error in creating student", e);
        }
    }
    public Subject getByName(String subjectName) throws DAOException {
        Subject subject;
        try {
            Query query = session.createSQLQuery("SELECT SUBJECT_NAME, ID FROM SUBJECT WHERE SUBJECT_NAME = :paramName AND NOT DELETED").addEntity(Subject.class);
            query.setParameter("paramName", subjectName);
            subject = (Subject) query.uniqueResult();
        } catch (Exception e) {
            throw new DAOException("Error in getting subject by name", e);
        }
        return subject;
    }
    public void close() throws DAOException {
        try {
            session.close();
        } catch (Exception e) {
            throw new DAOException("Resources is not closed", e);
        }
    }
}