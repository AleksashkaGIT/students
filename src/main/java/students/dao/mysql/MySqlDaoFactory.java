package students.dao.mysql;
import students.dao.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
public class MySqlDaoFactory extends DAOFactory{
	@Autowired
	private SessionFactory sessionFactory;
    private Session session;
    public Session getSession() throws DAOException {
        try {
            session = sessionFactory.openSession();
        } catch (Exception e) {
            throw new DAOException("Error of session", e);
        }
        return session;
    }
     public StudentDao getStudentDAO(Session session){
        return new MySqlStudentDao(session);
    }
    public SubjectDao getSubjectDAO(Session session){
        return new MySqlSubjectDao(session);
    }
    public MarkDao getMarkDAO(Session session){
        return new MySqlMarkDao(session);
    }
}