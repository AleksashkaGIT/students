package students.dao;
import students.domain.Student;
import java.util.List;
public interface StudentDao {
    void create(Student student)throws DAOException;
    Student getByPK(int key)throws DAOException;
    void update(Student student)throws DAOException;
    void delete(int key)throws DAOException;
    List<Student> getAll()throws DAOException;
    void close()throws DAOException;
}