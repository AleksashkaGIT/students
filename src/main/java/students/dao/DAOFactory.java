package students.dao;
import org.hibernate.Session;
public abstract class DAOFactory {
    public abstract Session getSession()throws DAOException;
    public abstract StudentDao getStudentDAO(Session session);
    public abstract MarkDao getMarkDAO(Session session);
    public abstract SubjectDao getSubjectDAO(Session session);
}