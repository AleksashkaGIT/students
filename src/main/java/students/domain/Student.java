package students.domain;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
@Where(clause = "deleted = false")
@Table(name="student")
@SQLDelete(sql = "UPDATE STUDENT SET DELETED = 1 WHERE ID = ?")
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, name="ID")
    private int id;
    @Column(unique = false, name="FIRST_NAME")
    private String firstName;
    @Column(unique = false, name="SECOND_NAME")
    private String secondName;
    @Column(unique = false, name="BIRTH_DATE")
    private String birthDate;
    @Column(unique = false, name="ENTER_YEAR")
    private int enterYear;
    @OneToMany(mappedBy = "student", orphanRemoval=true, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Mark> marks;
    private Boolean deleted = false;
	public Student(){}
    public Student(int id, String firstName, String secondName, String birthDate, int enterYear){
	    this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.enterYear = enterYear;
    }
    public void setId (int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public String getFirstName(){
        return firstName;
    }
    public void setSecondName(String secondName){
        this.secondName = secondName;
    }
    public String getSecondName(){
        return secondName;
    }
    public void setBirthDate(String birthDate){
        this.birthDate = birthDate;
    }
    public String getBirthDate(){
        return birthDate;
    }
    public void setEnterYear(int enterYear){
        this.enterYear = enterYear;
    }
    public int getEnterYear(){
        return enterYear;
    }
    public void setMarks(List<Mark> marks){
        this.marks = marks;
    }
    public List<Mark> getMarks(){
        return marks;
    }
    public String toString() {
        return "Number of student: " + String.valueOf(id) + " , name: " + firstName +
                ", surname: " + secondName +
                ", date of birth: " + birthDate + ", year of admission: " + String.valueOf(enterYear);
    }
}