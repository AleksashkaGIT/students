package students.domain;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
@Where(clause = "deleted = false")
@Table(name = "subject")
@SQLDelete(sql = "UPDATE SUBJECT SET DELETED = 1 WHERE ID = ?")
public class Subject implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, name="ID")
    private int id;
    @Column(unique = true, name = "SUBJECT_NAME")
    private String subjectName;
    @OneToMany(mappedBy = "subject", orphanRemoval=true, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Mark> marks;
    @Column(name="DELETED")
    private Boolean deleted = false;
	public Subject(){}
	public Subject(int id, String subjectName){
        this.id = id;
        this.subjectName = subjectName;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSubjectName() {
        return subjectName;
    }
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
	  public void setMarks(List<Mark> marks){
        this.marks = marks;
    }
    public List<Mark> getMarks(){
        return this.marks;
    }
    public String toString() {
            return "Number of subject: " + String.valueOf(id) + " , subject: " + subjectName;
    }
}