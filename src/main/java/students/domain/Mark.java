package students.domain;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.io.Serializable;
@Entity
@Where(clause = "deleted = false")
@Table(name="mark")
@SQLDelete(sql = "UPDATE MARK SET DELETED = 1 WHERE ID = ?")
public class Mark implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, name="ID")
    private int id;
    @ManyToOne
    @JoinColumn(name = "STUDENT_ID")
    private Student student;
    @ManyToOne
    @JoinColumn(name = "SUBJECT_ID")
    private Subject subject;
    @Column(unique = false, name = "MARK")
    private int mark;
    @Column(name="DELETED")
    private Boolean deleted = false;
	public Mark(){}
	public Mark(int id, Student student, Subject subject, int mark){
        this.id = id;
        this.student = student;
        this.subject = subject;
        this.mark = mark;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getMark() {
        return mark;
    }
    public void setMark(int mark) {
        this.mark = mark;
    }
    public void setStudent(Student student) {
        this.student = student;
    }
    public Student getStudent(){
        return student;
    }
    public void setSubject(Subject subject) {
        this.subject = subject;
    }
    public Subject getSubject() {
        return subject;
    }
    public String toString() {
            return " Mark: " + String.valueOf(mark);
    }
}