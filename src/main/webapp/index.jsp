<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML5>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
  <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div class="main">
    <div class="header">
        <div class="logo">
            <div class="logo_text">
                <h1><a href="index.html">StUdEnTs</a></h1>
                <h2>Ученье - свет, а за свет надо платить</h2>
            </div>
        </div>
        <div class="menubar">
            <ul class="menu">
				<li> <form action="/studentsApp/index.jsp">
                        <input type = "submit" class="button" value = "Main page StUdEnTs">
                     </form>
                </li>
                <li> <form action="servlet" method="POST">
                        <input type = "submit" class="button" name = "button" value = "Edit the list of students">
                     </form>
                </li>
                <li> <form action="servlet" method="POST">
                        <input type = "submit" class="button" name = "button" value = "Edit the list of subjects">
                     </form>
                </li>
             </ul>
        </div>
    </div>
    <div class="site_content">
        <h1> Содержимое контента </h1>
</div>
    <div class="footer">
        <p>website.by 2018</p>
    </div>
</body>
</html>