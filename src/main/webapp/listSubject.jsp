<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<html>
<head>
    <title>All subjects</title>
</head>
<body>
<h2>All subjects</h2>
<table border=\"2\">
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Update subject</th>
        <th>Delete subject</th>
    </tr>
    <c:forEach items="${listSubject}" var="subject">
        <tr>
            <td>${subject.getId()}</td>
            <td>${subject.getSubjectName()}</td>
            <td> <form action="updateSubject.jsp">
                <input type="hidden" name = "id" value="${subject.getId()}">
                <input type="hidden" name = "name" value="${subject.getSubjectName()}">
                <input type= "submit" value= "Update subject">
            </form>
            </td>
            <td><form method= "POST">
                <input type="hidden" name = "idSub" value="${subject.getId()}">
                <input type= "submit" name = "button" value= "Delete subject">
            </form>
            </td>
        </tr>
    </c:forEach>
    <form action = "addSubject.jsp">
        <input type = "submit" value="Add subject">
    </form>
</table>
</body>
</html>