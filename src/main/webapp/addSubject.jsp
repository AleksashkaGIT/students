<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<style type="text/css">
    input[type="text"]:invalid:not(:placeholder-shown){border:2px solid red;}
</style>
<html>
<head>
    <title>Add Subject</title>
</head>
<body>
<h1><% if (request.getAttribute("messageAddSub") != null) {
        out.print(request.getAttribute("messageAddSub"));
    } %>
</h1>
<h2>Add subject</h2>
<form action = "servlet" method = "POST" >
    <input type = "text" name="subjectname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct subject name!')" oninput="setCustomValidity('')" placeholder="Subject name" required><br>
    <input type="submit" name ="button" value="Add subject">
</form>
<h2>Return to:</h2>
<form action = "servlet" method = "POST" >
    <input type = "submit" name ="button" value = "Edit the list of subjects">
</form>
</body>
</html>