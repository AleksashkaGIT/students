<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<style type="text/css">
    input[type="text"]:invalid:not(:placeholder-shown){border:2px solid red;}
</style>
<html>
<head>
    <title>Add Student</title>
</head>
<body>
<script type="text/javascript" src="js/script.js"></script>
<h1 id="result"></h1>
<h2>Add student</h2>
<form name="form1" onsubmit="event.preventDefault();">
    <input type = "text" id="fname" name="firstname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct student name!')" oninput="setCustomValidity('')" placeholder="Student name" required><br>
    <input type = "text" id="sname" name="secondname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct student surname!')" oninput="setCustomValidity('')" placeholder="Surname" required><br>
    <input type = "text" id="birthday" name="birthday" pattern = "((19|20)[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])" oninvalid="this.setCustomValidity('Please, input correct student date of birth: year-mm-dd, for example: 2018-04-30!')" oninput="setCustomValidity('')" placeholder="Date of Birth: year-mm-dd" required><br>
    <input type = "text" id="year" name="year" pattern = "(19|20)[0-9]{2}" oninvalid="this.setCustomValidity('Please, input correct year, for example: 2018!')" oninput="setCustomValidity('')" placeholder="Year of admission" required><br>
    <input type="submit" id="button" value="Add student" onclick="addStudent()">
</form>
<h3>Return to: </h3>
<form action = "servlet"  method = "POST" >
    <input type = "submit" name = "button" value = "Edit the list of students">
</form>
</body>
</html>