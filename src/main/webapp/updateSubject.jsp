<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<style type="text/css">
    input[type="text"]:invalid:not(:placeholder-shown){border:2px solid red;}
	.hide { display: none; }
	.show { display: block; }
</style>
<html>
<head>
    <title>Update Subject</title>
</head>
<body>
<h1><% if (request.getAttribute("messageUpdSub") != null) {
        out.print(request.getAttribute("messageUpdSub"));
    } %>
</h1>
<div class="${messageUpdSub != null ? 'hide' : 'show'}">
<h2>Update subject</h2>
<form action = "servlet"  method = "POST" >
    <input type="hidden" name = "idSubj" required value="${param.id}"><br>
    <input type = "text" name="subjectname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct subject name!')" oninput="setCustomValidity('')" placeholder="${param.name}" required><br>
    <input type="submit" name ="button" value="Update subject">
</form>
</div>
<h2>Return to:</h2>
<form action = "servlet"  method = "POST" >
    <input type = "submit" name = "button" value = "Edit the list of subjects">
</form>
</body>
</html>