<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<style type="text/css">
    input[type="text"]:invalid:not(:placeholder-shown){border:2px solid red;}
</style>
<html>
<head>
    <title>Update student</title>
	<script type='text/javascript' src='js/jquery-3.3.1.js'></script>
	<script type="text/javascript" src="js/jQueryScript.js"></script>
</head>
<body>
<h1 id="result"></h1>
<h2>Update student</h2>
<form id="form">
    <input type="hidden" id = "idStudent" required value="${param.id}"><br>
    <input type = "text" id="firstname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct student name!')" oninput="setCustomValidity('')" placeholder="${param.name}" required><br>
    <input type = "text" id="secondname" pattern = "[A-Za-z]{2,}" oninvalid="this.setCustomValidity('Please, input correct student surname!')" oninput="setCustomValidity('')" placeholder="${param.surname}" required><br>
    <input type = "text" id="birthday" pattern = "((19|20)[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])" oninvalid="this.setCustomValidity('Please, input correct student date of birth: year-mm-dd, for example: 2018-04-30!')" oninput="setCustomValidity('')" placeholder="${param.birth}" required><br>
    <input type = "text" id="year" pattern = "(19|20)[0-9]{2}" oninvalid="this.setCustomValidity('Please, input correct year, for example: 2018!')" oninput="setCustomValidity('')" placeholder="${param.year}" required><br>
    <input type="submit" id ="button" value="Update student">
</form>
</div>
<h2>Return to:</h2>
<form action = "servlet"  method = "POST" >
    <input type = "submit" name = "button" value = "Edit the list of students">
</form>
</body>
</html>

