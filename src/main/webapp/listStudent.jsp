<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ page errorPage="errorPage.jsp" %>
<!DOCTYPE HTML5>
<html>
<head>
    <title>All Students</title>
</head>
<body>
<h2>All students</h2>
<table border=\"2\">
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Date of Birth</th>
        <th>Year of admission</th>
        <th>Update</th>
        <th>Delete student</th>
        <th>Appoint subject</th>
    </tr>
    <c:forEach items="${listStudent}" var="student">
        <tr>
            <td>${student.getId()}</td>
            <td>${student.getFirstName()}</td>
            <td>${student.getSecondName()}</td>
            <td>${student.getBirthDate()}</td>
            <td>${student.getEnterYear()}</td>
            <td>
                <form action="updateStudent.jsp">
                    <input type="hidden" name = "id" value="${student.getId()}">
                    <input type="hidden" name = "name" value="${student.getFirstName()}">
                    <input type="hidden" name = "surname" value="${student.getSecondName()}">
                    <input type="hidden" name = "birth" value="${student.getBirthDate()}">
                    <input type="hidden" name = "year" value="${student.getEnterYear()}">
                    <input type= "submit" value= "Update student">
                </form>
            </td>
            <td>
                <form method= "POST">
                    <input type="hidden" name = "idStud" value="${student.getId()}">
                    <input type= "submit" name = "button" value= "Delete student">
                </form>
            </td>
            <td>
                <form action="appointSubject.jsp">
                    <input type="hidden" name = "id" value="${student.getId()}">
                    <input type= "submit" value= "Appoint subject">
                </form>
            </td>
        </tr>
    </c:forEach>
    <form action = "addStudent.jsp">
        <input type = "submit" value="Add student">
    </form>
</table>
</body>
</html>

