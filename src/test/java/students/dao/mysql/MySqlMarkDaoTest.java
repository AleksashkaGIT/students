package students.dao.mysql;
import students.dao.*;
import students.domain.Mark;
import students.dao.mysql.MySqlDaoFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import java.util.List;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import org.hibernate.Session;
@ContextConfiguration("classpath:root-context-test.xml")
public class MySqlMarkDaoTest extends AbstractTestNGSpringContextTests {
	@Autowired
    private SessionFactory sessionFactory;
    private MarkDao markDao;
    private StudentDao studentDao;
    private SubjectDao subjectDao;
	@BeforeClass
    public void initAll() throws DAOException {
		MySqlDaoFactory daoFactory = new MySqlDaoFactory();
		Session session = sessionFactory.openSession();
		markDao = daoFactory.getMarkDAO(session);
		studentDao = daoFactory.getStudentDAO(session);
        subjectDao = daoFactory.getSubjectDAO(session);
    }
    @Test
    void createTest(){
        Mark expMark = null;
        try {
            expMark = new Mark(0, studentDao.getByPK(2), subjectDao.getByPK(2), 8);
            markDao.create(expMark);
            assertEquals(expMark.toString(), markDao.getByPK(expMark.getId()).toString());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                if(markDao.getByPK(expMark.getId()) != null) {
                    markDao.delete(expMark.getId()); //удалить созданный объект
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
    @Test
    public void updateTest(){
        Mark before = null;
        try {
            before = markDao.getByPK(1);
             Mark expMark = new Mark(1,studentDao.getByPK(1), subjectDao.getByPK(1), 4);
            markDao.update(expMark);
            assertEquals(expMark.toString(), markDao.getByPK(1).toString());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                if (before != null) {
                    markDao.update(before); //возврат изменений объекта
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
    @Test
    public void getByPkTest() throws DAOException{
        Mark mark = (Mark) markDao.getByPK(1);
        assertNotNull(mark);
        assertNotEquals(mark.toString(), markDao.getByPK(2).toString());
    }
    @Test
    public void getAllTest() throws DAOException{
        List<Mark> list = markDao.getAll();
        assertNotNull(list);
        assertEquals(4, list.size());
    }
    @Test
    public void deleteTest(){
        Mark before = null;
        try {
            before = markDao.getByPK(3);
            markDao.delete(3);
            assertNull(markDao.getByPK(3));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(before != null) {
                    markDao.update(before); //возврат поля deleted на 0(false)
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
   @AfterClass
    public void tearDownAll(){
        DAOException exc = null;
        try {
            studentDao.close();
        } catch (DAOException d){
            exc = d;
        }
        try {
            subjectDao.close();
        } catch (DAOException d){
            if(exc == null) {
                exc = d;
            } else {
                System.err.println("SubjectDao is not closed");
            }
        }
        try {
            markDao.close();
        } catch (DAOException d){
            if(exc == null) {
                exc = d;
            } else {
                System.err.println("MarkDao is not closed");
            }
        }
        if (exc != null) {
            System.err.println("Closing error");
            exc.printStackTrace();
        }
    }
}