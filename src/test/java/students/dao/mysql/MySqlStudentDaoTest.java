package students.dao.mysql;
import students.dao.*;
import students.domain.Student;
import students.dao.mysql.MySqlDaoFactory;
import java.util.List;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
@ContextConfiguration("classpath:root-context-test.xml")
public class MySqlStudentDaoTest extends AbstractTestNGSpringContextTests{
	@Autowired
    private SessionFactory sessionFactory;
	private StudentDao studentDao;
    @BeforeClass
    public void initAll() throws DAOException {
		MySqlDaoFactory daoFactory = new MySqlDaoFactory();
        studentDao = daoFactory.getStudentDAO(sessionFactory.openSession());
    }
    @Test
    public void createTest() {
        Student expStudent = new Student(0, "Ekaterina", "Sidorova", "1990-11-07", 2010);
        try {
            studentDao.create(expStudent);
            assertEquals(expStudent.toString(), studentDao.getByPK(expStudent.getId()).toString());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                if(studentDao.getByPK(expStudent.getId()) != null) {
                    studentDao.delete(expStudent.getId()); //удалить созданный объект
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
    @Test
    public void updateTest() {
        Student before = null;
        try {
            before = studentDao.getByPK(1);
            Student expStudent = new Student(1, "Igor", "Ivanov", "1991-05-02", 2008);
            studentDao.update(expStudent);
            assertEquals(expStudent.toString(), studentDao.getByPK(1).toString());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                if (before != null) {
                    studentDao.update(before); //возврат изменений объекта
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
    @Test
    public void getByPkTest() throws DAOException{
        Student student = studentDao.getByPK(1);
        assertNotNull(student);
        assertNotEquals(student.toString(), studentDao.getByPK(2).toString());
    }
    @Test
    public void getAllTest() throws DAOException{
        List<Student> list = studentDao.getAll();
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    @Test
    public void deleteTest(){
        Student before = null;
        try {
            before = studentDao.getByPK(1);
            studentDao.delete(1);
            assertNull(studentDao.getByPK(1));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(before != null) {
                    studentDao.update(before); //возврат поля deleted на 0(false)
                }
            }catch (DAOException d){
                d.printStackTrace();
            }
        }
    }
    @AfterClass
    public void tearDownAll(){
        try {
            if (studentDao != null) {
                studentDao.close();
            }
        } catch (DAOException d) {
            System.err.println("Closing studentDao error");
            d.printStackTrace();
        }
    }
}