package students.dao.mysql;
import students.dao.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.testng.annotations.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import static org.testng.Assert.assertNotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Session;
@ContextConfiguration("classpath:root-context-test.xml")
public class MySqlDaoFactoryTest extends AbstractTestNGSpringContextTests{
	@Autowired
    private SessionFactory sessionFactory;
    @Test
    void connectionTest() throws DAOException {
		Session session = sessionFactory.openSession();
        assertNotNull(session, "Connection failed");
    }
}