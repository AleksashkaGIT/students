package students.dao.mysql;
import students.dao.*;
import students.dao.mysql.MySqlDaoFactory;
import students.domain.Subject;
import java.util.List;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
@ContextConfiguration("classpath:root-context-test.xml")
public class MySqlSubjectDaoTest extends AbstractTestNGSpringContextTests{
	@Autowired
    private SessionFactory sessionFactory;
    private SubjectDao subjectDao;
	@BeforeClass
    public void initAll() throws DAOException {
		MySqlDaoFactory daoFactory = new MySqlDaoFactory();
        subjectDao = daoFactory.getSubjectDAO(sessionFactory.openSession());
    }
    @Test
    public void createTest() {
        Subject expSubject = new Subject(0, "History");
        try {
            subjectDao.create(expSubject);
            assertEquals(expSubject.toString(), subjectDao.getByPK(expSubject.getId()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (subjectDao.getByPK(expSubject.getId()) != null) {
                    subjectDao.delete(expSubject.getId()); //удалить созданный объект
                }
            } catch (DAOException d) {
                d.printStackTrace();
            }
        }
    }
    @Test
    public void updateTest() {
        Subject before = null;
        try {
            before = subjectDao.getByPK(1);
            Subject expSubject = new Subject(1, "Chemistry");
            subjectDao.update(expSubject);
            assertEquals(expSubject.toString(), subjectDao.getByPK(1).toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (before != null) {
                    subjectDao.update(before);
                }
            } catch (DAOException d) {
                d.printStackTrace();
            }
        }
    }
    @Test
    public void getByPkTest()throws DAOException{
        Subject subject = subjectDao.getByPK(1);
        assertNotNull(subject);
        assertNotEquals(subject.toString(), subjectDao.getByPK(2).toString());
    }
    @Test
    public void getAllTest() throws DAOException{
        List<Subject> list = subjectDao.getAll();
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    @Test
    public void getByNameTest()throws DAOException{
        Subject subject = subjectDao.getByName("Math");
        assertNotNull(subject);
        assertNotEquals(subject.toString(), subjectDao.getByName("English"));
    }
    @Test
    public void deleteTest(){
        Subject before = null;
        try {
            before = subjectDao.getByPK(1);
            subjectDao.delete(1);
            assertNull(subjectDao.getByPK(1));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (before != null) {
                    subjectDao.update(before); //возврат поля deleted на 0(false)
                }
            } catch (DAOException d) {
                d.printStackTrace();
            }
        }
    }
    @AfterClass
    public void tearDownAll(){
        try {
            if (subjectDao != null) {
                subjectDao.close();
            }
        } catch (DAOException d) {
            System.err.println("Closing subjectDao error");
            d.printStackTrace();
        }
    }
}